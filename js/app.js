const rowInfo = document.getElementById('row-info');

async function API(){
    try {
        let response = await fetch('https://aves.ninjas.cl/api/birds')
        let resultJson = await response.json()
        console.log(resultJson);
        return resultJson;
    } catch (err) {
        console.log("Error: ", err)
    }
}

const cardsTemplate = (info) =>{
    let cards = '';
    info.forEach(element => {
        cards += `
            <div class="col s12 m4">
                <div class="card medium">
                    <div class="card-image">
                        <img src="${element.images.main}">
                        <span class="card-title">${element.name.spanish}</span>
                    </div>
                    <div class="card-content">
                        <p>
                            Inglés: ${element.name.english}
                        </p>
                        <p>
                            Latin: ${element.name.latin}
                        </p>
                    </div>
                </div>
            </div>
        `;
    });
    rowInfo.innerHTML = cards;
}

const errorTemplate = () => {
    let text = '';
    text = `<h2>Surgió un error al cargar la información :(</h2>`;
    rowInfo.innerHTML = text;
}

document.addEventListener('DOMContentLoaded', () => {
    const elems = document.querySelectorAll('.sidenav');
    const instances = M.Sidenav.init(elems);
    
    API().then(data => {
        cardsTemplate(data);
    })
    .catch(errorTemplate());
});